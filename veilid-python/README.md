# Veilid Bindings for Python

## Usage

To use:
```
poetry add veilid
```
or 
```
pip3 install veilid
```


## Development

To run tests:
```
poetry run pytest
```

To update schema for validation with the latest copy from a running `veilid-server`:
```
./update_schema.sh
```
